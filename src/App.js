import React from 'react';
import Nav from './components/Nav'
import ClubList from './components/ClubList'
import PlayerList from './components/PlayerList'
import PlayerDetail from './components/PlayerDetail'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

function App() {
  return (
    <Router>
      <Nav/>
      <Switch>
        <Route exact path='/clubs' component={ClubList} />
        <Route exact path='/players' component={PlayerList} />
        <Route exact path='/playerDetail/:playerId' component={PlayerDetail} />
        <Route render={() => <h1>404</h1> } />
      </Switch>
    </Router>
  );
}

export default App;
