import React from 'react'
import { Container, Header} from 'semantic-ui-react'

export default function ClubDetail({ club }) {

  return (
    <Container>
      <Header as='h1' floated='left'>{club.name}</Header>
      <img src={club.logoUrl}/>
    </Container>
  )

}