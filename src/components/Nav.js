import React from 'react'
import { NavLink } from 'react-router-dom'
import { Menu } from 'semantic-ui-react'

export default function Nav () {
  const [activeItem, setActiveItem] = React.useState('')

  const handleItemClick = (e, { name }) => setActiveItem(name)

  return (
    <Menu stackable>
      <Menu.Item>
        <img src='http://turnajelevu.cz/wp-content/uploads/elementor/thumbs/logo-okaaxexcp945dyvun435jraj7ymc2bf6fdc882fm68.png' />
      </Menu.Item>
      <Menu.Item 
        as={NavLink} 
        to='/clubs'
        exact
        name='clubs'
        active={activeItem === 'clubs'}
        onClick={handleItemClick}
      >
        Clubs
      </Menu.Item>
      <Menu.Item
        as={NavLink} 
        to='/players'
        exact
        name='players'
        active={activeItem === 'players'}
        onClick={handleItemClick}
      >
        Players
      </Menu.Item>
    </Menu>
  )
}