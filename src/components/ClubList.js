import React from 'react'
import { getAllClubs, updateClub, deleteClub } from '../utils/api'
import { ErrorMessage, DeleteDialog } from '../utils/modals'
import Loading from './Loading'
import { TextInput } from './commonComponents'
import { Container, Table, Image, Button, Header, List, Modal} from 'semantic-ui-react'
import { Formik, Form } from 'formik'

function ClubRow({ club, expand, handleDelete }) {
  const [clubState, setClubState] = React.useState(club)
  const [expandState, setExpandState] = React.useState(!!expand)
  const [showDeleteConfirm, setShowDeleteConfirm] = React.useState(false)
  const [error, setError] = React.useState(null)

  const callDelete = (club) => {
    deleteClub(club.id)
    .then(() => {
      setError(null)
      handleDelete()
    })
    .catch(({ message }) => {
      setError(message)
    })
  }

  const handleConfirm = (confirmed) => {
    if (confirmed) {
      callDelete(clubState)
    }
    setShowDeleteConfirm(false)
  }

  return (
    <React.Fragment>

        { error && <ErrorMessage open={true} message={error} /> }

        { showDeleteConfirm &&
          <DeleteDialog
            obj={clubState}
            objectType='Club'
            open={true}
            handleConfirm={handleConfirm}
          />
        }

      <Table.Row>
          <Table.Cell><Image src={clubState.logoUrl} size='mini' /></Table.Cell>
          <Table.Cell>{clubState.name}</Table.Cell>
          <Table.Cell>
            {(expandState === false)
              ? <ClubTeamsColumn teams={clubState.teams} />
              : <List items={clubState.teams.map((team) => (team.name))} />
            }
          </Table.Cell>
          <Table.Cell  textAlign='right'>
            <Button
              circular
              icon='info'
            />
            <ClubEditFormModal
              club={clubState}
              icon='edit'
              circular
              onSubmit={(club) => setClubState(club)} />
            <Button
              circular
              icon='trash'
              onClick={() => setShowDeleteConfirm(true)}
            />
          </Table.Cell>
          <Table.Cell  textAlign='right'>
            {(expandState === false)
              ? <Button circular icon='chevron down' onClick={() => setExpandState(true)} />
              : <Button circular icon='chevron up' onClick={() => setExpandState(false)} />
            }
          </Table.Cell>
        </Table.Row>
    </React.Fragment>
  )
}

function ClubTeamsColumn({ teams }) {
  switch (teams.length) {
    case 0:
      return '-'
    case 1:
      return `1 team (${teams[0].name})`
    default:
      return `${teams.length} teams`
  }
}

function ClubEditFormModal({ club, onSubmit, ...props }) {
  const [showModal, setShowModal] = React.useState(false)
  const [error, setError] = React.useState(null)

  const closeModal = () => {
    setShowModal(false)
  }

  const handleReset = (resetForm) => {
    closeModal()
    resetForm()
  }

  const handleSubmit = (data, resetForm) => {
    club.name = data.name
    club.logoUrl = data.logoUrl

    updateClub(club)
    .then((club) => {
      onSubmit(club)
      closeModal()
      resetForm()
    })
    .catch(({ message }) => {
      setError(message)
    })
  }

  return (
    <Formik
      initialValues={{
        name: club.name || '',
        logoUrl: club.logoUrl || ''
      }}
      validate={(values) => {
        const errors = {}

        if (values.name === '') {
          errors.name = "Required";
        }

        return errors
      }}
      onSubmit={(data, { resetForm }) => {
        handleSubmit(data, resetForm)
      }}
    >
      {({ resetForm }) => (
        <Modal
          as={Form}
          open={showModal}
          trigger={<Button
          {...props}
          onClick={() => setShowModal(true)}/>}
          className='form'
         >
            <Modal.Content>
              { error && <p>{error}</p> }
              <TextInput
                required
                name='name'
                type='input'
                label='Name'
              />
              <TextInput
                name='logoUrl'
                type='input'
                label='Logo URL'
              />
            </Modal.Content>
            <Modal.Actions>
              <Button icon="times" content="Cancel" onClick={handleReset.bind(null, resetForm)} />
              <Button type="submit" color="green" icon="save" content="Save" />
            </Modal.Actions>
        </Modal>
      )}
    </Formik>
  )

}

function clubListReducer(state, action) {
  if (action.type === 'success') {
    return {
      clubs: action.clubs,
      error: null,
      reload: false,
      loading: false
    }
  } else if (action.type === 'error') {
    return {
      ...state,
      error: action.error.message,
      reload: false,
      loading: false
    }
  } else if (action.type === 'reload') {
    return {
      ...state,
      loading: true,
      reload: true
    }
  } else {
    throw new Error(`That action type is not supported.`)
  }
}

export default function ClubList() {
  const [state, dispatch] = React.useReducer(
    clubListReducer,
    { clubs: [], error: null, loading: true, reload: true }
  )

  React.useEffect(() => {
    if (state.reload === true) {
      getAllClubs()
      .then((clubs) => dispatch({ type: 'success', clubs}))
      .catch((error) => dispatch({ type: 'error', error}))}
  })

  const handleDelete = () => {
    dispatch({type: 'reload'})
  }

  const { clubs, error, loading } = state

  if (loading === true) {
    return <Loading/>
  }

  if (error) {
    return <p>{error}</p>
  }

  return (
    <Container>
      <div>
        <Header as='h1' floated='left'>Clubs List</Header>
        <ClubEditFormModal
          floated='right'
          club={{}}
          color='green'
          icon='plus'
          circular
          onSubmit={(club) => {
            if (!!club.id) {
              clubs.push(club)
              dispatch({type: 'reload'})
            }
            }}
          />
      </div>
      <Table striped>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Logo</Table.HeaderCell>
            <Table.HeaderCell>Name</Table.HeaderCell>
            <Table.HeaderCell>Teams</Table.HeaderCell>
            <Table.HeaderCell />
            <Table.HeaderCell />
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {clubs.map((club) => {
            return (
              <ClubRow 
                key={club.id}
                club={club}
                handleDelete={handleDelete}
              />
            )
          })}
        </Table.Body>
      </Table>
    </Container>
  )
}