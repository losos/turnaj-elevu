import React from 'react'
import { getPlayer } from '../utils/api'
import Loading from './Loading'
import { Container, Image } from 'semantic-ui-react'

export default function PlayerDetail(props) {
  const { playerId } = props.match.params
  const [state, dispatch] = React.useReducer(
    playerListReducer,
    { player: {}, error: null, loading: true, reload: true }
  )

  function playerListReducer(state, action) {
    if (action.type === 'success') {
      console.log('podarilo sa nacitat playera')
      return {
        player: action.player,
        error: null,
        loading: false
      }
    } else if (action.type === 'error') {
      console.log('error pri nacitani playera')
      return {
        ...state,
        error: action.error.message,
        loading: false
      }
    } else {
      throw new Error(`That action type is not supported.`)
    }
  }

  React.useEffect(() => {
    console.log('effect', playerId)
    if (playerId) {
      getPlayer(playerId)
        .then((player) => {dispatch({ type: 'success', player})})
        .catch((error) => dispatch({ type: 'error', error}))
    }
  }, [])

  const { player, error, loading } = state

  if (loading === true) {
    return <Loading/>
  }

  if (error) {
    return <p>{error}</p>
  }

  return (
    <Container>
      <h1>{`${player.firstname} ${player.surname}`}</h1>
      <Image src={player.fotoUrl} size='small' verticalAlign='top' />
    </Container>
  )
}