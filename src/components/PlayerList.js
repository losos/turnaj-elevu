import React from 'react'
import { getAllPlayers, updatePlayer, deletePlayer, getPlayerFormData, getPlayer } from '../utils/api'
import { ErrorMessage, DeleteDialog } from '../utils/modals'
import Loading from './Loading'
import { Container, Form, Table, Image, Input, Button, Header, Dropdown, Modal, Checkbox} from 'semantic-ui-react'
import { useForm, Controller } from 'react-hook-form';
import { DateInput } from 'semantic-ui-calendar-react';
import { Link } from 'react-router-dom'

function PlayerEditFormModal({ playerId, onSubmit, formData, handleClose, ...props }) {
  const [showModal, setShowModal] = React.useState(props.open)
  const [player, setPlayer] = React.useState({})
  const [callUpdatePlayer, setCallUpdatePlayer] = React.useState(false)
  const [loading, setLoading] = React.useState(false)
  const { register, control, handleSubmit, setValue, errors, watch, reset } = useForm({defaultValues: {
    stickHolding: player.stickHolding || '',
    positions: player.positions || [],
    active: player.active || false,
    birthdate: player.birthdate || '',
  }});
  const watchBirthdate = watch("birthdate", player.birthdate);

  const onFormSubmit = data => {
    player.active = data.active
    player.firstname = data.firstname
    player.surname = data.surname
    player.nickname = data.nickname
    player.birthdate = data.birthdate
    player.fotoUrl = data.fotoUrl
    player.number = data.number
    player.height = data.height
    player.weight = data.weight
    player.stickHolding = data.stickHolding || null
    player.positions = data.positions
    setCallUpdatePlayer(true)
}

  const closeModal = () => {
    setShowModal(false)
    handleClose()
  }

  const handlePositionsChange = (event, data) => {
    setValue('positions', data.value);
  }

  const handleStickHoldingChange = (event, data) => {
    setValue('stickHolding', data.value);
  }

  const handleActiveChange = (event, data) => {
    setValue('active', data.checked);
  }
  
  const handleBirthdateChange = (event, data) => {
    setValue('birthdate', data.value)
  }

  React.useEffect(() => {
    setShowModal(props.open)
  }, [props.open])

  React.useEffect(() => {
    if (playerId > 0 && showModal === true) {
      setLoading(true)
      getPlayer(playerId)
        .then((fetchedPlayer) => {
          reset(fetchedPlayer)
          setPlayer(fetchedPlayer)
          setLoading(false)
        })
        .catch(({ message }) => {
          setLoading(false)
          console.log('error', message)
        })
    } else {
      setPlayer({})
      reset({})
    }
  },[showModal])

  React.useEffect(() => {
    register({ name: 'positions' })
    register({ name: 'stickHolding' })
    register({ name: 'active' })
    register({ name: 'birthdate' })
  }, [register])

  React.useEffect(() => {
    if (callUpdatePlayer === true) {
      updatePlayer(player)
      .then((player) => {
        closeModal()
        onSubmit(player)
      })
      .catch(({ message }) => {
        console.log('error', message)
      })
    }
  }, [callUpdatePlayer])

  if (loading === true) {
    return <Modal><Loading/></Modal>
  }

  return (
    <Modal
      as={Form}
      open={showModal}
      trigger={<Button
        {...props}
        onClick={() => {
          setShowModal(true)
          setCallUpdatePlayer(false)
        }}
      />}
      onSubmit={handleSubmit(onFormSubmit)}
      noValidate
    >
      <Modal.Content>
        <Controller
          as={<Form.Field control={Input}/>}
          defaultValue={player.firstname || ''}
          control={control}
          label='First name'
          name='firstname'
        />
        <Controller
          as={<Form.Field control={Input}/>}
          defaultValue={player.surname || ''}
          control={control}
          label='Surname'
          name='surname'
          rules={{ required: true }}
          error={errors.surname && {
            content: 'Surname is required',
          }}
          required
        />
        <Controller
          as={<Form.Field control={Input}/>}
          defaultValue={player.nickname || ''}
          control={control}
          label='Nickname'
          name='nickname'
        />
        <Controller
          as={<Form.Field control={Input}/>}
          defaultValue={player.fotoUrl || ''}
          control={control}
          label='Foto URL'
          name='fotoUrl'
        />
        <Controller
          as={<Form.Field control={Input}/>}
          defaultValue={player.number || ''}
          control={control}
          label='Number'
          name='number'
        />
        <Controller
          as={<Form.Field control={Input}/>}
          defaultValue={player.height || ''}
          control={control}
          label='Height (cm)'
          name='height'
        />
        <Controller
          as={<Form.Field control={Input}/>}
          defaultValue={player.weight || ''}
          control={control}
          label='Weight (kg)'
          name='weight'
        />
        <Form.Field
          control={DateInput}
          value={watchBirthdate || ''}
          label='Birthdate'
          name='birthdate'
          animation=''
          iconPosition='left'
          onChange={handleBirthdateChange}
          format='DD-MM-YYYY'
          closable
        />
        <Form.Field
          control={Dropdown}
          defaultValue={player.stickHolding || ''}
          placeholder='Select'
          fluid selection 
          options={formData.stickHoldingOptions}
          label='Stick holding'
          name='stickHolding'
          onChange={handleStickHoldingChange}
        />
        <Form.Field
          control={Dropdown}
          defaultValue={player.positions || []}
          fluid multiple selection 
          options={formData.playerPositionOptions}
          label='Positions'
          name='positions'
          onChange={handlePositionsChange}
        />
        <Form.Field
          control={Checkbox}
          defaultValue={player.active || false}
          defaultChecked={player.active || false}
          toggle
          label='Active'
          name='active'
          onChange={handleActiveChange}
        />
      </Modal.Content>
      <Modal.Actions>
        <Button icon="times" content="Cancel" onClick={closeModal} />
        <Button type="submit" color="green" icon="save" content="Save" />
      </Modal.Actions>
  </Modal>
  )
}

function PlayerRow({ player, handleDelete, handleEdit }) {
  const [playerState, setPlayerState] = React.useState(player)
  const [showDeleteConfirm, setShowDeleteConfirm] = React.useState(false)
  const [callDeletePlayer, setCallDeletePlayer] = React.useState(false)
  const [error, setError] = React.useState(null)

  React.useEffect(() => {
    if (callDeletePlayer === true) {
      deletePlayer(playerState.id)
      .then(() => {
        setError(null)
        handleDelete()
      })
      .catch(({ message }) => {
        setError(message)
      })
    }
  }, [callDeletePlayer])

  const handleConfirm = (confirmed) => {
    if (confirmed) {
      setCallDeletePlayer(true)
    }
    setShowDeleteConfirm(false)
  }

  if (error) {
    return <ErrorMessage open={true} message={error} />
  }

  if (showDeleteConfirm === true) {
    return <DeleteDialog
              obj={playerState}
              objectType='Player'
              open={true}
              handleConfirm={handleConfirm}
            />
  }

  return (
    <React.Fragment>
      <Table.Row>
          <Table.Cell><Image src={playerState.fotoUrl} size='mini' /></Table.Cell>
          <Table.Cell>
            <Link exact to={`/playerDetail/${playerState.id}`}>
              {`${playerState.firstname} ${playerState.surname}`}
            </Link> 
          </Table.Cell>
          <Table.Cell>{playerState.nickname}</Table.Cell>
          <Table.Cell>{playerState.number}</Table.Cell>
          <Table.Cell  textAlign='right'>
            <Button 
              as={Link} 
              exact
              to={`/playerDetail/${playerState.id}`}
              circular
            >
              Detail
            </Button>
            <Button
              circular
              icon='edit'
              onClick={() => {handleEdit(playerState.id)}}
            />
            <Button
              circular
              icon='trash'
              onClick={() => setShowDeleteConfirm(true)}
            />
          </Table.Cell>
        </Table.Row>
    </React.Fragment>
  )
}

function playerListReducer(state, action) {
  if (action.type === 'success') {
    return {
      players: action.players,
      error: null,
      reload: false,
      loading: false
    }
  } else if (action.type === 'error') {
    return {
      ...state,
      error: action.error.message,
      reload: false,
      loading: false
    }
  } else if (action.type === 'reload') {
    return {
      ...state,
      loading: true,
      reload: true
    }
  } else {
    throw new Error(`That action type is not supported.`)
  }
}

export default function PlayerList() {
  const formData = React.useRef({})
  const [playerIdToEdit, setPlayerIdToEdit] = React.useState(0)
  const [state, dispatch] = React.useReducer(
    playerListReducer,
    { players: [], error: null, loading: true, reload: true }
  )

  React.useEffect(() => {
    if (state.reload === true) {
      getAllPlayers()
      .then((players) => dispatch({ type: 'success', players}))
      .catch((error) => dispatch({ type: 'error', error}))}
  })

    React.useEffect(() => {
    getPlayerFormData()
    .then((data) => {
      formData.current = data
    })
  }, [])

  const handleDelete = () => {
    dispatch({type: 'reload'})
  }

  const { players, error, loading } = state

  if (loading === true) {
    return <Loading/>
  }

  if (error) {
    return <p>{error}</p>
  }

  return (
    <Container>
      <div>
        <Header as='h1' floated='left'>Players List</Header>
        <PlayerEditFormModal
          open={playerIdToEdit > 0}
          floated='right'
          playerId={playerIdToEdit}
          color='green'
          icon='plus'
          circular
          formData={formData.current}
          onSubmit={() => {
            dispatch({type: 'reload'})
          }}
          handleClose={() => setPlayerIdToEdit(0)}
        />
      </div>
      <Table striped>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Foto</Table.HeaderCell>
            <Table.HeaderCell>Name</Table.HeaderCell>
            <Table.HeaderCell>Nickname</Table.HeaderCell>
            <Table.HeaderCell>Number</Table.HeaderCell>
            <Table.HeaderCell />
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {players.map((player) => {
            return (
              <PlayerRow 
                key={player.id}
                player={player}
                formData={formData.current}
                handleDelete={handleDelete}
                handleEdit={(playerId) => {setPlayerIdToEdit(playerId)}}
              />
            )
          })}
        </Table.Body>
      </Table>
    </Container>
  )
}