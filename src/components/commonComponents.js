import React from 'react'
import PropTypes from 'prop-types'
import { useField } from 'formik'
import { Form, Dropdown } from 'semantic-ui-react'

export function TextInput({label, required, ...props}) {
  const [field, meta] = useField(props);
  const errorText = meta.error && meta.touched ? meta.error : ''
  const error = errorText ? {content: errorText} : false
  return (
    <Form.Field
      {...field}
      required={required}
      control='input'
      label={label}
      error={error}
    />
  )
}

TextInput.propTypes = {
  label: PropTypes.string,
  required: PropTypes.bool
}

export function DropdownMultipleSelection({placeholder, options, label, ...props}) {
  console.log('props', props)

  return (
      <Form.Field 
        as={Dropdown}
        fluid multiple selection
        {...props}
        placeholder={placeholder}
        options={options} />
  )

  
}