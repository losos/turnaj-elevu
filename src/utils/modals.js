import React from 'react'
import { Modal, Button } from 'semantic-ui-react'

export function DeleteDialog({ open, objectType, handleConfirm }) {
  const [show, setShow] = React.useState(open)

  const close = () => {
    setShow(false)
  }

  const handleDialogConfirm = (result) => {
    close()
    handleConfirm(result)
  }

  return (
    <Modal
      open={open}
    >
      <Modal.Header>{`Delete ${objectType}`}</Modal.Header>
      <Modal.Content>
        <p>{`Are you sure you want to delete this ${objectType}?`}</p>
      </Modal.Content>
      <Modal.Actions>
        <Button onClick={() => {handleDialogConfirm(false)}} negative>
          No
        </Button>
        <Button
          onClick={() => {handleDialogConfirm(true)}}
          positive
          icon='checkmark'
          content='Yes'
        />
      </Modal.Actions>
    </Modal>
  )
}
/*
export class DeleteDialog extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      open: props.open,
      objType: props.objectType,
      obj: props.obj
    }

  }

  close = () => {
    this.setState({ 
      open: false
    })
  }

  handleConfirm = (result) => {
    this.close()
    this.props.handleConfirm(result)
  }

  render() {
    const { open } = this.state
    const { objType } = this.props

    return (
      <Modal
        open={open}
      >
        <Modal.Header>{`Delete ${objType}`}</Modal.Header>
        <Modal.Content>
          <p>{`Are you sure you want to delete this ${objType}?`}</p>
        </Modal.Content>
        <Modal.Actions>
          <Button onClick={() => {this.handleConfirm(false)}} negative>
            No
          </Button>
          <Button
            onClick={() => {this.handleConfirm(true)}}
            positive
            icon='checkmark'
            content='Yes'
          />
        </Modal.Actions>
      </Modal>
    )
  }
}
*/
export class ErrorMessage extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      open: true,
      message: props.message
    }

  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      open: true
    });
  }

  close = () => this.setState({ 
    open: false
  })

  render() {
    const { open, message } = this.state

    return (
      <Modal
        open={open}
      >
        <Modal.Header>Error!</Modal.Header>
        <Modal.Content>
          <p>{message}</p>
        </Modal.Content>
        <Modal.Actions>
        <Button
            onClick={this.close}
            content='Ok'
            positive
          />
        </Modal.Actions>
      </Modal>
    )
  }
}