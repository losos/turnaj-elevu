const API_BASE_URL = 'http://localhost:8181/api'

export function getAllClubs() {
  return fetch(`${API_BASE_URL}/clubs`)
  .then((response) => response.json())
  .then((clubs) => {
    if (clubs.message) {
      throw new Error(clubs.message)
    }

    return clubs;
  })
}

export function updateClub(club) {
  const options = {
    method: 'PUT',
    body: JSON.stringify(club),
    headers: {
      'Content-type': 'application/json; charset=UTF-8'
    }
  }

  return fetch(`${API_BASE_URL}/clubs`, options)
  .then(response => response.json())
  .then(club => {
    if (club.message) {
      throw new Error(club.message)
    }

    if (!club.teams) {
      club.teams = []
    }

    return club
  })
}

export function deleteClub(clubId) {
  const options = {
    method: 'DELETE',
    headers: {
      'Content-type': 'application/json; charset=UTF-8'
    }
  }

  return fetch(`${API_BASE_URL}/clubs/${clubId}`, options)
  .then(response => {
    if (!response.ok) {
      throw new Error(`An error occured. (Status : ${response.status}).`)
    }

    return response
  })
}

export function getAllPlayers() {
  return fetch(`${API_BASE_URL}/players`)
  .then((response) => response.json())
  .then((players) => {
    if (players.message) {
      throw new Error(players.message)
    }

    return players;
  })
}

export function getPlayer(id) {
  return fetch(`${API_BASE_URL}/players/${id}`)
  .then(response => response.json())
  .then(player => {
    if (player.message) {
      throw new Error(player.message)
    }

    return player;
  })
}

export function updatePlayer(player) {
  const options = {
    method: 'PUT',
    body: JSON.stringify(player),
    headers: {
      'Content-type': 'application/json; charset=UTF-8'
    }
  }

  return fetch(`${API_BASE_URL}/players`, options)
  .then(response => response.json())
  .then(player => {
    if (player.message) {
      throw new Error(player.message)
    }

    return player
  })
}

export function deletePlayer(playerId) {
  const options = {
    method: 'DELETE',
    headers: {
      'Content-type': 'application/json; charset=UTF-8'
    }
  }

  return fetch(`${API_BASE_URL}/players/${playerId}`, options)
  .then(response => {
    if (!response.ok) {
      throw new Error(`An error occured. (Status : ${response.status}).`)
    }

    return response
  })
}

export function getPlayerFormData() {
  return fetch(`${API_BASE_URL}/playerFormData`)
  .then((response) => response.json())
  .then((data) => {
    if (data.message) {
      throw new Error(data.message)
    }

    return data;
  })
}