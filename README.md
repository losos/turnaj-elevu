<h1>About Project</h1>

This project is web application for administrating kids floorbal tournament Turnaj Elévů (http://turnajelevu.cz/). It can be used for any other tournament too. 

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

<h1>Screenshots</h1>
<h3>Players List</h3>
![Players List](img/screenshots/playersList.png)
<h3>Player form</h3>
![Player Form](img/screenshots/playerForm.png)